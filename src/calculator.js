class Calculator {
  calculate(x, y, operator) {
    if (operator === '+') {
      return x + y;
    } 
    else if (operator === '-') {
      return x - y;
    } 
    else if (operator === 'x') {
      return x * y;
    } 
    else if (operator === '/') {
      return x / y;
    }
  }
}

module.exports = Calculator;
