const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const Calculator = require('../../../src/calculator.js');

Given('an operation', () => {
  this.calculator = new Calculator();
});

When('the user calculates {int} {string} {int}', (x, operator, y) => {
  this.result = this.calculator.calculate(x, y, operator);
});

Then('the result should be {int}', (expectedResult) => {
  assert.deepEqual(this.result, expectedResult);
});
