Feature: Calculator
  As an user
  I want to calculate my expenses

  Scenario Outline: Basic operations
    Given an operation
    When the user calculates <x> <operator> <y>
    Then the result should be <result>

  Examples:
  | x | operator | y | result |
  | 2 | "+"      | 2 | 4      |
  | 5 | "+"      | 5 | 10     |
  | 5 | "-"      | 3 | 2      |
  | 2 | "x"      | 5 | 10     |
  | 8 | "/"      | 2 | 4      |